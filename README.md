# bigclock


Displays a big clock to the window and add visual arc.

This way you can compare synchronization between multiple PCs visually.


The project is implemented using Qt Quick having QML code.


## setup development


sudo apt install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools qtdeclarative5-dev
sudo apt-get install qml-module-qtquick-controls qml-module-qtquick-shapes

## build

qmake bigclock.pro
make
