import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    id:window
    width: 640
    height: 480
    visible: true
    title: qsTr("big clock")
    color: "black"

    Text {
        color: "yellow"
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        id: timeStr
        text: "result"
        font.family: "Monospaced"
        font.pixelSize: Math.min(window.height, window.width)/8
        font.weight: Font.Bold

    }
    BackDraw {
        id: arc

        thickness: 4
        // rotation: angleSpinner.value
    }
    Timer {
        interval: 20; running: true; repeat: true
        onTriggered: {
            var datetime = new Date()
            timeStr.text = Qt.formatTime(datetime, "hh:mm:ss")
            arc.rotation = 360 * datetime.getMilliseconds() / 1000
        }
    }
}
