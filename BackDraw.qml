import QtQuick 2.15
import QtQuick.Shapes 1.15

Item {
    id: root
    property int radius: Math.min(width/2, height/2)
    property int thickness: 10

    width: parent.width
    height: parent.height
    //x: parent.top
    //y: parent.left
    anchors.centerIn: parent

    function centerX() { return root.width/2 }
    function centerY() { return root.height/2 }

/*
    Rectangle {
        border.color: "red"
        border.width: 10
        anchors.fill: parent
    }
*/
//     onRadiusChanged: console.log(radius)
    Shape {

//    anchors.centerIn: parent


    ShapePath {

        strokeWidth: 2
        strokeColor: "transparent"
        fillGradient: ConicalGradient {
            centerX: root.width/2; centerY: root.height/2
            angle: 90
            // x1: 20; y1: 20
            // x2: 180; y2: 130
            GradientStop { position: 0; color: "yellow" }
            // GradientStop { position: 0.2; color: "green" }
            // GradientStop { position: 0.4; color: "red" }
            // GradientStop { position: 0.6; color: "yellow" }
            GradientStop { position: 1; color: "black" }
        }
        // strokeStyle: ShapePath.DashLine
        // dashPattern: [ 1, 4 ]
        startX: centerX(); startY: centerY()-root.radius
        /*
        PathLine { x: 180; y: 130 }
        PathLine { x: 20; y: 130 }
        PathLine { x: 20; y: 20 }
        */
        PathArc {
            x: centerX()+root.radius; y: centerY()
            radiusX: root.radius; radiusY: root.radius
            useLargeArc: true
            direction: PathArc.Counterclockwise
        }
        PathLine {
            x: centerX()+root.radius-root.radius/root.thickness
            y: centerY()
        }
        PathArc {
            x: centerX()
            y: centerY()-root.radius+root.radius/root.thickness
            radiusX: root.radius-root.radius/root.thickness; radiusY: root.radius-root.radius/root.thickness
            useLargeArc: true
        }
        PathLine {
            x: centerX()
            y: centerY()-root.radius
        }
    }
}
}
